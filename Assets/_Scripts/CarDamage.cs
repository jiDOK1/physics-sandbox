﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarDamage : MonoBehaviour
{
    public SkinnedMeshRenderer doorFR;
    float damageTime = 0.05f;
    float damageTimer = 0f;
    bool isDamaging = false;

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            isDamaging = true;
        }

        if (isDamaging)
        {
            damageTimer += Time.deltaTime;
            float t = damageTimer / damageTime;
            float blendShapeValue = Mathf.Lerp(0, 100f, t);
            doorFR.SetBlendShapeWeight(0, blendShapeValue);
            if (damageTimer >= damageTime)
            {
                isDamaging = false;
            }
        }
    }
}
