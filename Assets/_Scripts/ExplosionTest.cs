﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionTest : MonoBehaviour
{
    [SerializeField]private float explosionStrength = 20f;
    [SerializeField]private Transform explosionPos;
    [SerializeField]private float explosionRadius = 2f;
    private Rigidbody[] rigidbodies;

    void Start()
    {
        rigidbodies = GetComponentsInChildren<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            for (int i = 0; i < rigidbodies.Length; i++)
            {
                rigidbodies[i].AddExplosionForce(explosionStrength, explosionPos.position, explosionRadius, 1f);
                //rigidbodies[i].
            }
        }
    }

    private void OnDrawGizmos()
    {
        if(explosionPos == null) { return; }
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(explosionPos.position, explosionRadius);
    }
}
