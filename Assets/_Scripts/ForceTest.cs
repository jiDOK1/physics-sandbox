﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class ForceTest : MonoBehaviour
{
    [SerializeField] private float strength;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            //rb.AddForce(Vector3.up * strength, ForceMode.Impulse);
            //rb.velocity = Vector3.up * strength;
            rb.AddForceAtPosition(Vector3.right * strength, transform.position + Vector3.up * 0.7f);
        }
    }

    void FixedUpdate()
    {
        //rb.AddForce(Vector3.up * strength, ForceMode.Acceleration);
    }
}
