﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityTest : MonoBehaviour
{
    [SerializeField] private Transform gravStart;
    [SerializeField] private Transform gravEnd;
    [SerializeField] private float gravStrength = 9.81f;
    private Vector3 gravDir = new Vector3(0f, -9.81f, 0f);

    void Update()
    {
        gravDir = gravEnd.position - gravStart.position;
        gravDir = gravDir.normalized;
        Debug.DrawRay(gravStart.position, gravDir, Color.green);
        Debug.DrawRay(gravStart.position, gravDir * gravStrength, Color.red);

        if (Input.GetButtonDown("Jump"))
        {
            Physics.gravity = gravDir;
        }
    }
}
