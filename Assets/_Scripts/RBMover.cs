﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBMover : MonoBehaviour
{
    [SerializeField] float speed = 20f;
    [SerializeField] float rotSpeed = 10f;
    private Rigidbody rb;
    private Vector3 velocity;
    private float fwd;
    private float rot;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        fwd = Input.GetAxis("Vertical");
        rot = Input.GetAxis("Horizontal");
    }

    private void FixedUpdate()
    {
        velocity = transform.forward * fwd * speed;
        velocity = new Vector3(velocity.x, -9.81f, velocity.z);
        rb.velocity = velocity;
        Quaternion deltaRot = Quaternion.Euler(Vector3.up * rot * rotSpeed);
        rb.MoveRotation(rb.rotation * deltaRot);
    }
}
