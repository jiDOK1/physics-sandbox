using System;
using UnityEngine;

public class SACarControllerMinimalCopy : MonoBehaviour
{
    [SerializeField] private WheelCollider[] wheelColliders = new WheelCollider[4];
    [SerializeField] private GameObject[] wheelMeshes = new GameObject[4];
    [SerializeField] private Vector3 centerOfMassOffset;
    [SerializeField] private float maxSteerAngle;
    [Range(0, 1)] [SerializeField] private float steerHelper;
    [Range(0, 1)] [SerializeField] private float tractionControl;
    [SerializeField] private float fullTorqueOverAllWheels;
    [SerializeField] private float reverseTorque;
    [SerializeField] private float maxHandbrakeTorque;
    [SerializeField] private float downforce = 100f;
    [SerializeField] private float topspeed = 200f;
    [SerializeField] private static int noOfGears = 5;
    [SerializeField] private float revRangeBoundary = 1f;
    [SerializeField] private float slipLimit;
    [SerializeField] private float brakeTorque;

    private Quaternion[] wheelMeshLocalRots;
    private Vector3 prevPos, pos;
    private float steerAngle;
    private int curGear;
    private float gearFactor;
    private float oldRot;
    private float curTorque;
    private Rigidbody rb;
    private const float reversingThreshold = 0.01f;

    public bool Skidding { get; private set; }
    public float BrakeInput { get; private set; }
    public float CurSteerAngle { get { return steerAngle; } }
    public float CurSpeed { get { return rb.velocity.magnitude * 3.6f; } }
    public float MaxSpeed { get; private set; }
    public float AccelInput { get; private set; }

    private void Start()
    {
        wheelMeshLocalRots = new Quaternion[4];
        for (int i = 0; i < 4; i++)
        {
            wheelMeshLocalRots[i] = wheelMeshes[i].transform.localRotation;
        }
        wheelColliders[0].attachedRigidbody.centerOfMass = centerOfMassOffset;
        maxHandbrakeTorque = float.MaxValue;

        rb = GetComponent<Rigidbody>();
        curTorque = fullTorqueOverAllWheels - (tractionControl * fullTorqueOverAllWheels);
    }

    private void GearChanging()
    {
        float f = Mathf.Abs(CurSpeed / MaxSpeed);
        float upgearlimit = (1 / (float)noOfGears) * (curGear + 1);
        float downgearlimit = (1 / (float)noOfGears) * curGear;

        if (curGear > 0 && f < downgearlimit)
        {
            curGear--;
        }
        if (f < upgearlimit && (curGear < (noOfGears - 1)))
        {
            curGear++;
        }
    }

    private static float CurveFactor(float factor)
    {
        return 1 - (1 - factor) * (1 - factor);
    }

    private void CalculateGearFactor()
    {
        float f = (1 / (float)noOfGears);
        var targetGearFactor = Mathf.InverseLerp(f * curGear, f * (curGear + 1), Mathf.Abs(CurSpeed / MaxSpeed));
        gearFactor = Mathf.Lerp(gearFactor, targetGearFactor, Time.deltaTime * 5f);
    }

    private void CalculateRevs()
    {
        //for display/sound
    }

    public void Move(float steering, float accel, float footbrake, float handbrake)
    {
        for (int i = 0; i < 4; i++)
        {
            wheelColliders[i].GetWorldPose(out Vector3 pos, out Quaternion rot);
            wheelMeshes[i].transform.position = pos;
            wheelMeshes[i].transform.rotation = rot;
        }

        steering = Mathf.Clamp(steering, -1, 1);
        AccelInput = accel = Mathf.Clamp01(accel);
        BrakeInput = footbrake = -1 * Mathf.Clamp(footbrake, -1, 0);
        handbrake = Mathf.Clamp01(handbrake);

        steerAngle = steering * maxSteerAngle;
        wheelColliders[0].steerAngle = steerAngle;
        wheelColliders[1].steerAngle = steerAngle;
        SteerHelper();
        ApplyDrive(accel, footbrake);
        CapSpeed();
        if (handbrake > 0f)
        {
            var hbTorque = handbrake * maxHandbrakeTorque;
            wheelColliders[2].brakeTorque = hbTorque;
            wheelColliders[3].brakeTorque = hbTorque;
        }

        GearChanging();

        AddDownForce();
        TractionControl();
    }

    private void CapSpeed()
    {
        float speed = rb.velocity.magnitude * 3.6f;
        if (speed > topspeed)
        {
            rb.velocity = (topspeed / 3.6f) * rb.velocity.normalized;
        }
    }

    private void ApplyDrive(float accel, float footbrake)
    {
        float thrustTorque = accel * (curGear / 2f);
        wheelColliders[0].motorTorque = wheelColliders[1].motorTorque = thrustTorque;

        for (int i = 0; i < 4; i++)
        {
            if(CurSpeed>5 && Vector3.Angle(transform.forward, rb.velocity) < 50f)
            {
                wheelColliders[i].brakeTorque = brakeTorque * footbrake;
            }
            else if (footbrake > 0)
            {
                wheelColliders[i].brakeTorque = 0f;
                wheelColliders[i].motorTorque = -reverseTorque * footbrake;
            }
        }
    }

    private void SteerHelper()
    {
        for (int i = 0; i < 4; i++)
        {
            WheelHit wheelHit;
            wheelColliders[i].GetGroundHit(out wheelHit);
            if(wheelHit.normal == Vector3.zero)
            {
                return;
            }
        }

        if(Mathf.Abs(oldRot - transform.eulerAngles.y) < 10f)
        {
            var turnAdjust = (transform.eulerAngles.y - oldRot) * steerHelper;
            Quaternion velRot = Quaternion.AngleAxis(turnAdjust, Vector3.up);
            rb.velocity = velRot * rb.velocity;
        }
        oldRot = transform.eulerAngles.y;
    }

    private void AddDownForce()
    {
        wheelColliders[0].attachedRigidbody.AddForce(-transform.up * downforce * wheelColliders[0].attachedRigidbody.velocity.magnitude);
    }

    private void TractionControl()
    {
        WheelHit wheelHit;
        wheelColliders[0].GetGroundHit(out wheelHit);
        AdjustTorque(wheelHit.forwardSlip);
        wheelColliders[1].GetGroundHit(out wheelHit);
        AdjustTorque(wheelHit.forwardSlip);
        
    }

    private void AdjustTorque(float forwardSlip)
    {
        if(forwardSlip >= slipLimit && curTorque >= 0)
        {
            curTorque -= 10 * tractionControl;
        }
        else
        {
            curTorque += 10 * tractionControl;
            if (curTorque > fullTorqueOverAllWheels)
            {
                curTorque = fullTorqueOverAllWheels;
            }
        }
    }
}
