﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceTaxi : MonoBehaviour
{
    [SerializeField] private float thrust = 20f;
    [SerializeField] private float angle = 4f;
    private Transform cylinder;
    Rigidbody rb;

    void Start()
    {
        cylinder = transform.GetChild(0);
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.T))
        {
            rb.AddForce(cylinder.up * thrust);
        }
        float rot = Input.GetAxis("Horizontal") * angle;
        cylinder.Rotate(Vector3.right * rot);
    }
}
