﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorqueTest : MonoBehaviour
{
    private void Start()
    {
        GetComponent<Rigidbody>().centerOfMass = Vector3.forward * 1.2f;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            GetComponent<Rigidbody>().AddTorque(Vector3.right, ForceMode.Impulse);
        }

        if (Input.GetKeyDown(KeyCode.Z))
        {
            GetComponent<Rigidbody>().AddTorque(Vector3.forward, ForceMode.Impulse);
        }

        float degree = 360f;
        float radian = Mathf.Deg2Rad * degree;
        degree = Mathf.Rad2Deg * radian;
    }
}
